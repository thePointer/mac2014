package hr.asyncro.mac2014.presenter;

import android.view.View;
import android.widget.TextView;

import com.dmacan.lightandroid.presenter.LightAdapterItem;
import com.dmacan.lightandroid.util.LightFont;

import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.data.Concept;
import hr.asyncro.mac2014.data.Function;

/**
 * Created by David on 18.9.2014..
 */
public class IngredientPresenter implements LightAdapterItem {

    private Concept concept;
    private Function function;
    private int indentationLevel = 0;
    private IngredientPresenter root;
    private int position = 0;

    public IngredientPresenter() {
    }

    public IngredientPresenter(IngredientPresenter presenter) {
        if (presenter.getConcept() != null)
            this.concept = new Concept(presenter.getConcept());
        if (presenter.getFunction() != null)
            this.function = new Function(presenter.getFunction());
        this.indentationLevel = presenter.getIndentationLevel();
    }

    public IngredientPresenter(Concept concept) {
        this.concept = concept;
    }

    public IngredientPresenter(Function function) {
        this.function = function;
    }

    public Concept getConcept() {
        return concept;
    }

    public void setConcept(Concept concept) {
        this.concept = concept;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public int getIndentationLevel() {
        return indentationLevel;
    }

    public void setIndentationLevel(int indentationLevel) {
        this.indentationLevel = indentationLevel;
    }

    public IngredientPresenter getRoot() {
        return root;
    }

    public void setRoot(IngredientPresenter root) {
        if (root != null)
            this.indentationLevel = root.getIndentationLevel() + 1;
        this.root = root;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void display(View view, int position) {
        String label = (function != null) ? function.getFunctionName() : concept.getName();
        if (concept != null && concept.getValue() > 0)
            label += " (" + concept.getValue() + ") ";
        else if (function != null)
            label += ";";
        TextView txtLabel = (TextView) view.findViewById(R.id.txtIngredientLabel);
        LightFont.setFont("courier_prime.ttf", txtLabel);
        txtLabel.setText(label);
        view.setPadding(indentationLevel * 50, 0, 0, 0);
    }

    @Override
    public int provideItemLayoutRes() {
        return R.layout.item_ingredient;
    }

}
