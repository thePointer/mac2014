package hr.asyncro.mac2014.presenter;

import android.view.View;
import android.widget.TextView;

import com.dmacan.lightandroid.presenter.LightAdapterItem;
import com.dmacan.lightandroid.util.LightFont;

import java.util.Locale;

import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.data.Concept;

/**
 * Created by David on 17.9.2014..
 */
public class ConceptPresenter implements LightAdapterItem {

    private Concept concept;

    public ConceptPresenter() {

    }

    public ConceptPresenter(Concept concept) {
        this.concept = concept;
    }

    @Override
    public void display(View view, int position) {
        String text = concept.getName();
        ((TextView) view.findViewById(R.id.txtConceptLabel)).setText(text);
        LightFont.setFont(((TextView) view.findViewById(R.id.txtCircularBackground)));
        String icon = (text.length() > 1) ? text.substring(0, 1) : text;
        ((TextView) view.findViewById(R.id.txtConceptIcon)).setText(icon.toUpperCase(Locale.ENGLISH)); // Sets the icon to be the first letter of the name
    }

    @Override
    public int provideItemLayoutRes() {
        return R.layout.item_concept;
    }

    public Concept getConcept() {
        return concept;
    }

    public void setConcept(Concept concept) {
        this.concept = concept;
    }
}
