package hr.asyncro.mac2014.presenter;

import android.view.View;
import android.widget.TextView;

import com.dmacan.lightandroid.presenter.LightAdapterItem;
import com.dmacan.lightandroid.util.LightFont;

import java.util.Locale;

import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.data.Recipe;

/**
 * Created by David on 18.9.2014..
 */
public class RecipePresenter implements LightAdapterItem {

    private Recipe recipe;

    public RecipePresenter(Recipe recipe) {
        this.recipe = recipe;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void display(View view, int position) {
        String text = recipe.getName();
        ((TextView) view.findViewById(R.id.txtConceptLabel)).setText(text);
        LightFont.setFont(((TextView) view.findViewById(R.id.txtCircularBackground)));
        String icon = "";
        if (text != null) {
            icon = (text.length() > 1) ? text.substring(0, 1) : text;
        }
        ((TextView) view.findViewById(R.id.txtConceptIcon)).setText(icon.toUpperCase(Locale.ENGLISH)); // Sets the icon to be the first letter of the name
    }

    @Override
    public int provideItemLayoutRes() {
        return R.layout.item_concept;
    }
}
