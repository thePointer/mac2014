package hr.asyncro.mac2014.adapter;

import android.content.Context;

import com.dmacan.lightandroid.presenter.LightAdapter;

import hr.asyncro.mac2014.presenter.IngredientPresenter;

/**
 * Created by David on 18.9.2014..
 */
public class CookingAdapter extends LightAdapter {

    private int selectedItemPos = 0;

    public CookingAdapter(Context context) {
        super(context);
    }

    public void addIngredient(IngredientPresenter presenter) {
        if (presenter.getRoot() == null)
            addItem(presenter);
        else {
            int pos = -1;
            int rootPos = -1;
            for (int i = 0; i < dohvatiSve().size(); i++) {
                if (presenter.getRoot() == ((IngredientPresenter) getItem(i)).getRoot())
                    pos = i;
                if (presenter.getRoot() == ((IngredientPresenter) getItem(i)))
                    rootPos = i;
            }
            if (pos == -1)
                pos = rootPos;
            addAfter(pos, presenter);
        }
    }

    public void deleteItem(IngredientPresenter presenter) {
        for (int i = 0; i < dohvatiSve().size(); i++)
            if (getItem(i) == presenter) {
                dohvatiSve().remove(i);
                deleteChildren(i, presenter.getIndentationLevel());
            }
        notifyDataSetInvalidated();
        notifyDataSetChanged();
    }

    public IngredientPresenter getSelectedItem() {
        return (IngredientPresenter) dohvatiSve().get(selectedItemPos);
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItemPos = selectedItem;
    }

    public void deleteChildren(int position, int indentationLevel) {
        for (int i = position; i < dohvatiSve().size() && (indentationLevel < ((IngredientPresenter) dohvatiSve().get(i)).getIndentationLevel()); i++) {
            IngredientPresenter presenter = (IngredientPresenter) getItem(i);
            presenter.setIndentationLevel(presenter.getIndentationLevel() - 1);
        }
    }
}
