package hr.asyncro.mac2014.algorithms;

import hr.asyncro.mac2014.presenter.IngredientPresenter;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class AlgorithmsCollection {
    public static String CONCEPT_KEY="repeat";


    public static int getUCC(IngredientPresenter presenter) {
        if (presenter.getRoot() == null && presenter.getConcept() == null)
            return 1;
        else if (presenter.getRoot() == null && presenter.getConcept() != null) {
            return presenter.getConcept().getValue();
        }
        if (presenter.getConcept() != null && presenter.getConcept().getName().equals(CONCEPT_KEY))
            return presenter.getConcept().getValue() + getUCC(presenter.getRoot());
        return getUCC(presenter.getRoot());
    }
}
