package hr.asyncro.mac2014.api;

import com.dmacan.lightandroid.api.LightRequest;

import hr.asyncro.mac2014.data.Recipe;
import hr.asyncro.mac2014.data.responses.ConceptResponse;
import hr.asyncro.mac2014.data.responses.ConceptsResponse;
import hr.asyncro.mac2014.data.responses.RecipeResponse;
import hr.asyncro.mac2014.data.responses.RecipeSubmitionResponse;
import hr.asyncro.mac2014.data.responses.RecipesResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by ahuskano on 9/17/2014.
 */
public interface MacAPI {

    public static String API_LOCATION = "http://95.85.4.199/mac/index.php";

    @GET("/concepts")
    void getConcepts(Callback<ConceptsResponse> response);

    @GET("/concepts/{id}")
    void getConcept(@Path("id") int id,Callback<ConceptResponse> response);

    @GET("/recepies")
    void getRecipes(Callback<RecipesResponse> response);

    @GET("/recepies/{id}")
    void getRecipe(@Path("id") int id, Callback<RecipeResponse> response);

    @POST("/recepies")
    void submitRecipe(@Body LightRequest request, Callback<RecipeSubmitionResponse> response);

}
