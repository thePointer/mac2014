package hr.asyncro.mac2014;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.dmacan.lightandroid.ui.drawer.DrawerData;
import com.dmacan.lightandroid.ui.drawer.DrawerItem;
import com.dmacan.lightandroid.ui.drawer.DrawerItemHeading;
import com.dmacan.lightandroid.ui.drawer.LightDrawerActivity;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.ArrayList;
import java.util.List;

import hr.asyncro.mac2014.fragments.AboutFragment;
import hr.asyncro.mac2014.fragments.CookFragment;
import hr.asyncro.mac2014.fragments.GlossaryFragment;
import hr.asyncro.mac2014.fragments.LeaderboardFragment;
import hr.asyncro.mac2014.fragments.ProgressFragment;

/**
 * Created by David on 17.9.2014..
 */
public class MainActivity extends LightDrawerActivity {

    @Override
    public List<DrawerItem> provideDrawerItems() {
        DrawerData.context = getBaseContext();
        List<DrawerItem> items = new ArrayList<DrawerItem>();
//        items.add(DrawerData.newItem(R.string.di_progress, R.string.ic_area_chart));
        items.add(new DrawerItemHeading(getResources().getString(R.string.app_name), getResources().getColor(R.color.background), getResources().getDrawable(R.drawable.ic_launcher_web)));
        items.add(DrawerData.newItem(R.string.di_cook, R.string.ic_birthday_cake));
        items.add(DrawerData.newItem(R.string.di_glossary, R.string.ic_book));
        items.add(DrawerData.newItem(R.string.di_leaderboard, R.string.ic_trophy));
        items.add(DrawerData.newItem(R.string.fragment_about, R.string.ic_info_circle));
        return items;
    }

    @Override
    protected int getDragMode() {
        return MenuDrawer.MENU_DRAG_WINDOW;
    }

    @Override
    protected Position getDrawerPosition() {
        return Position.LEFT;
    }

    @Override
    public void main(Bundle savedInstanceState, boolean drawerActivity) {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public Fragment provideFragment(String tag) {
        if (tag.equals(getResources().getString(R.string.di_cook)) || tag.equals("default"))
            return new CookFragment();
        if (tag.equals(getResources().getString(R.string.di_progress)))
            return new ProgressFragment();
        else if (tag.equals(getResources().getString(R.string.di_leaderboard)))
            return new LeaderboardFragment();
        else if (tag.equals(getResources().getString(R.string.di_glossary)))
            return new GlossaryFragment();
        else if (tag.equals(getResources().getString(R.string.fragment_about)))
            return new AboutFragment();
        return null;
    }

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_drawer;
    }
}
