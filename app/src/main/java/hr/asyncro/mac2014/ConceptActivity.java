package hr.asyncro.mac2014;

import com.dmacan.lightandroid.LightActivity;

import hr.asyncro.mac2014.fragments.ConceptFragment;

/**
 * Created by David on 17.9.2014..
 */
public class ConceptActivity extends LightActivity {
    @Override
    public int provideLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    public void main() {
        setupFragment(R.id.container, new ConceptFragment());
    }
}
