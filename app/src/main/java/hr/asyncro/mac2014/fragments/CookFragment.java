package hr.asyncro.mac2014.fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dmacan.lightandroid.LightFragment;
import com.dmacan.lightandroid.api.LightResponse;
import com.dmacan.lightandroid.api.listener.OnDataReadListener;
import com.dmacan.lightandroid.api.listener.OnErrorListener;
import com.dmacan.lightandroid.ui.drawer.LightDrawerActivity;
import com.dmacan.lightandroid.util.LightFont;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import hr.asyncro.mac2014.MainActivity;
import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.adapter.CookingAdapter;
import hr.asyncro.mac2014.algorithms.AlgorithmsCollection;
import hr.asyncro.mac2014.data.Concept;
import hr.asyncro.mac2014.data.Function;
import hr.asyncro.mac2014.data.Recipe;
import hr.asyncro.mac2014.data.controllers.RecipeController;
import hr.asyncro.mac2014.data.responses.RecipeSubmitionResponse;
import hr.asyncro.mac2014.data.responses.RecipesResponse;
import hr.asyncro.mac2014.presenter.IngredientPresenter;
import hr.asyncro.mac2014.presenter.RecipePresenter;
import hr.asyncro.mac2014.widget.ChooseActionDialog;
import hr.asyncro.mac2014.widget.CookDialog;
import hr.asyncro.mac2014.widget.InfoDialog;
import hr.asyncro.mac2014.widget.SubmitDialog;
import hr.asyncro.mac2014.widget.ValueDialog;
import retrofit.RetrofitError;

/**
 * Created by David on 17.9.2014..
 */
public class CookFragment extends LightFragment implements OnDataReadListener, OnErrorListener, CookDialog.OnRecipeSelectListener, CookDialog.OnIngredientSelectListener, View.OnClickListener, AdapterView.OnItemClickListener, ChooseActionDialog.OnActionChosenListener, SubmitDialog.OnButtonClickListener, ValueDialog.OnValueConfirmListener, LightDrawerActivity.OnMenuItemSelectedListener, InfoDialog.OnInfoConfirmListener {

    @InjectView(R.id.lvIngredients)
    ListView lvIngredients;
    private CookingAdapter adapter;
    private RecipeController recipeController;
    private CookDialog recipeDialog;
    private CookDialog ingredientDialog;
    private SubmitDialog submitDialog;
    private ChooseActionDialog choiceDialog;
    private ValueDialog valueDialog;
    private InfoDialog infoDialog;
    private Recipe chosenRecipe;
    private View footerView;
    private boolean submit = false;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_cook;
    }

    @Override
    public void main() {
        readRecipes();
        valueDialog = new ValueDialog(getLightActivity());
        valueDialog.setOnValueConfirmListener(this);
        choiceDialog = new ChooseActionDialog(getLightActivity());
        choiceDialog.setOnActionChosenListener(this);
        recipeDialog = new CookDialog(getLightActivity());
        recipeDialog.setOnRecipeSelectListener(this);
        submitDialog = new SubmitDialog(getLightActivity());
        submitDialog.setOnButtonClickListener(this);
        ingredientDialog = new CookDialog(getLightActivity());
        ingredientDialog.setOnIngredientSelectListener(this);
        infoDialog = new InfoDialog(getLightActivity());
        infoDialog.setOnInfoConfirmListener(this);
        initList();
    }

    private void initList() {
        adapter = new CookingAdapter(getLightActivity());
        LayoutInflater inflater = (LayoutInflater) getLightActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footerView = inflater.inflate(R.layout.item_ingredient_placeholder, null, false);
        footerView.setOnClickListener(this);
        LightFont.setFont(((TextView) footerView.findViewById(R.id.txtPlaceholderIcon)));
        lvIngredients.addFooterView(footerView);
        lvIngredients.setAdapter(adapter);
        lvIngredients.setOnItemClickListener(this);
    }

    private void readRecipes() {
        submit = false;
        recipeController = new RecipeController();
        recipeController.readRecipes();
        recipeController.setOnDataReadListener(this);
        recipeController.setOnErrorListener(this);
        getLightMessenger().showLoading(getResources().getString(R.string.message_loading_content));
    }

    @Override
    public void onDataRead(LightResponse response) {
        if (!submit) {
            RecipesResponse res = (RecipesResponse) response;
            getLightMessenger().dismissAll();
            recipeDialog.populateDialog(res.getData());
            recipeDialog.show();
        } else {
            RecipeSubmitionResponse res = (RecipeSubmitionResponse) response;
            getLightMessenger().showSuccess(res.getMessage() + " " + res.getPoints() + " points");

        }
    }

    @Override
    public void onError(RetrofitError error) {
        getLightMessenger().showError(error.getMessage());
    }

    @Override
    public void onIngredientSelect(IngredientPresenter ingredientPresenter) {
        adapter.addIngredient(ingredientPresenter);
    }

    @Override
    public void onRecipeSelect(RecipePresenter recipePresenter) {
        adapter.clear();
        chosenRecipe = recipePresenter.getRecipe();
        ingredientDialog.clear();
        ingredientDialog.populateDialog(chosenRecipe);
    }

    @Override
    public void onClick(View view) {
        if (chosenRecipe == null)
            recipeDialog.show();
        else
            ingredientDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (((IngredientPresenter) adapter.getItem(i)).getConcept() != null) {
            adapter.setSelectedItem(i);
            choiceDialog.setSelectedPresenter((IngredientPresenter) adapter.getItem(i));
            choiceDialog.show();
        } else if (((IngredientPresenter) adapter.getItem(i)).getFunction() != null)
            adapter.deleteItem((IngredientPresenter) adapter.getItem(i));
    }

    @Override
    public void onActionChosen(ChooseActionDialog.Action action) {
        switch (action) {
            case ADD:
                ingredientDialog.setIngredientSelected(choiceDialog.getSelectedPresenter());
                ingredientDialog.show();
                break;
            case WRITE:
                valueDialog.show();
                break;
            case DELETE:
                adapter.deleteItem(choiceDialog.getSelectedPresenter());
                break;
        }
    }

    @Override
    public void onButtonClickListener(String user) {
        submit = true;
        submitRecipes(user);
    }

    private void submitRecipes(String user) {
        List<Function> functions = new ArrayList<Function>();
        List<Concept> concepts = new ArrayList<Concept>();

        for (int i = 0; i < adapter.getCount(); i++) {
            IngredientPresenter presenter = (IngredientPresenter) adapter.getItem(i);
            if (presenter.getFunction() != null) {
                Function function = presenter.getFunction();
                function.setUsageCount(AlgorithmsCollection.getUCC(presenter));
                functions.add(function);
            }
        }
        recipeController.submitRecipe(chosenRecipe, functions, concepts, user);
    }


    @Override
    public void onValueConfirm(String value) {
        try {
            adapter.getSelectedItem().getConcept().setValue(Integer.parseInt(value));
            adapter.notifyDataSetInvalidated();
        } catch (Exception e) {
            getLightMessenger().showError(getResources().getString(R.string.error_integer_value));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cook, menu);
        setMenu(menu);
        supplyMenuIcons(R.color.text_inv, R.string.ic_arrow_circle_o_up, R.string.ic_trash, R.string.ic_lightbulb, R.string.ic_repeat, R.string.ic_plus_circle);
        ((MainActivity) getLightActivity()).setOnMenuItemSelectedListener(this);
    }


    @Override
    public void onMenuItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_ingredient:
                if (chosenRecipe == null)
                    getLightMessenger().showError("You haven't selected any recipe");
                else
                    ingredientDialog.show();
                break;
            case R.id.action_change_recipe:
                recipeDialog.show();
                break;
            case R.id.action_clear_list:
                adapter.clear();
                break;
            case R.id.action_info:
                infoDialog.setInfo(chosenRecipe.getDescription());
                infoDialog.show();
                break;
            case R.id.action_submit:
                submitDialog.show();
                break;
        }
    }

    @Override
    public void onInfoConfirmListener() {
        infoDialog.dismiss();
    }
}
