package hr.asyncro.mac2014.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dmacan.lightandroid.LightFragment;
import com.dmacan.lightandroid.api.LightResponse;
import com.dmacan.lightandroid.api.listener.OnDataReadListener;
import com.dmacan.lightandroid.api.listener.OnErrorListener;
import com.dmacan.lightandroid.presenter.LightAdapter;
import com.dmacan.lightandroid.util.LightAPIUtil;

import butterknife.InjectView;
import hr.asyncro.mac2014.ConceptActivity;
import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.data.Concept;
import hr.asyncro.mac2014.data.controllers.ConceptController;
import hr.asyncro.mac2014.data.responses.ConceptsResponse;
import hr.asyncro.mac2014.presenter.ConceptPresenter;
import hr.asyncro.mac2014.util.Const;
import retrofit.RetrofitError;

/**
 * Created by David on 17.9.2014..
 */
public class GlossaryFragment extends LightFragment implements OnDataReadListener, OnErrorListener, AdapterView.OnItemClickListener {

    @InjectView(R.id.lvConcepts)
    ListView lvConcepts;

    private ConceptController conceptController;
    private LightAdapter adapter;
    private boolean conceptsReaderActivated = false;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_glossary;
    }

    @Override
    public void main() {
        adapter = new LightAdapter(getLightActivity());
        lvConcepts.setAdapter(adapter);
        lvConcepts.setOnItemClickListener(this);
        conceptController = new ConceptController();
        conceptController.readConcepts();
        conceptController.setOnDataReadListener(this);
        conceptController.setOnErrorListener(this);
        getLightMessenger().showLoading("Loading concepts");
    }

    @Override
    public void onDataRead(LightResponse response) {
        ConceptsResponse conceptResponse = (ConceptsResponse) response;
        getLightMessenger().showSuccess("Concepts loaded");
        if (conceptResponse != null && conceptResponse.getData() != null)
            fillAdapter(conceptResponse.getData());
        else
            getLightMessenger().showError("Reading of concepts failed");
    }

    @Override
    public void onError(RetrofitError error) {
        getLightMessenger().showError(error.getMessage());
    }

    private void fillAdapter(Concept[] concepts) {
        for (Concept concept : concepts)
            adapter.addItem(new ConceptPresenter(concept));
        //adapter.addItem(new ConceptPresenter(concept));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Concept selectedConcept = ((ConceptPresenter) adapter.getItem(i)).getConcept();
        Intent intent = new Intent(getLightActivity(), ConceptActivity.class);
        intent.putExtra(Const.Key.CONCEPT_CURRENT, LightAPIUtil.createGson().toJson(selectedConcept));
        startActivity(intent);
    }
}
