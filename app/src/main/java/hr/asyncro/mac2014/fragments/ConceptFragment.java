package hr.asyncro.mac2014.fragments;

import android.widget.TextView;

import com.dmacan.lightandroid.LightFragment;
import com.dmacan.lightandroid.util.LightAPIUtil;
import com.dmacan.lightandroid.util.LightFont;

import java.util.Locale;

import butterknife.InjectView;
import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.data.Concept;
import hr.asyncro.mac2014.util.Const;

/**
 * Created by David on 17.9.2014..
 */
public class ConceptFragment extends LightFragment {

    @InjectView(R.id.txtConceptIcon)
    TextView icConcept;
    @InjectView(R.id.txtConceptDescriptionIcon)
    TextView icConceptDescription;
    @InjectView(R.id.txtConceptImplementationIcon)
    TextView icConceptImplementation;
    @InjectView(R.id.txtConceptDescription)
    TextView txtConceptDescription;
    @InjectView(R.id.txtConceptName)
    TextView txtConceptName;
    @InjectView(R.id.txtConceptImplementation)
    TextView txtConceptImplementation;
    @InjectView(R.id.txtConceptCategory)
    TextView txtConceptCategory;

    private Concept concept;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_concept;
    }

    @Override
    public void main() {
        concept = LightAPIUtil.createGson().fromJson(getLightActivity().getIntent().getExtras().getString(Const.Key.CONCEPT_CURRENT), Concept.class);
        LightFont.setFont(icConcept, icConceptDescription, icConceptImplementation);
        txtConceptDescription.setText(concept.getDescription());
        txtConceptImplementation.setText(concept.getExample());
        txtConceptName.setText(concept.getName().toUpperCase(Locale.ENGLISH));
        txtConceptCategory.setText(concept.getCategoryName());
    }

}
