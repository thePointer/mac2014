package hr.asyncro.mac2014.fragments;

import android.webkit.WebView;

import com.dmacan.lightandroid.LightFragment;

import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.widget.MACWebViewClient;

/**
 * Created by David on 17.9.2014..
 */
public class LeaderboardFragment extends LightFragment {

    private String URL = "http://95.85.4.199/mac/";
    private WebView webView;

    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_leaderboard;
    }

    @Override
    public void main() {
        webView = (WebView) getView().findViewById(R.id.wvHighScores);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MACWebViewClient());
        webView.loadUrl(URL);
    }

}
