package hr.asyncro.mac2014.fragments;

import com.dmacan.lightandroid.LightFragment;

import hr.asyncro.mac2014.R;

/**
 * Created by David on 17.9.2014..
 */
public class ProgressFragment extends LightFragment {
    @Override
    public int provideLayoutRes() {
        return R.layout.fragment_progress;
    }

    @Override
    public void main() {
        getLightMessenger().showSuccess("Progress fragment");
    }
}
