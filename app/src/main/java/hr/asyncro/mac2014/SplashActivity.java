package hr.asyncro.mac2014;

import com.dmacan.lightandroid.LightSplashActivity;


public class SplashActivity extends LightSplashActivity {

    private static final int SPLASH_TIME = 1500;

    @Override
    public int provideLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public int getSplashTime() {
        return SPLASH_TIME;
    }

    @Override
    public Class getNextClassActivity() {
        return MainActivity.class;
    }
}
