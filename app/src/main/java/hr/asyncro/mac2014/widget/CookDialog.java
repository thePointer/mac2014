package hr.asyncro.mac2014.widget;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.dmacan.lightandroid.presenter.LightAdapter;
import com.dmacan.lightandroid.ui.widget.LightDialog;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;

import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.data.Concept;
import hr.asyncro.mac2014.data.Function;
import hr.asyncro.mac2014.data.Recipe;
import hr.asyncro.mac2014.presenter.IngredientPresenter;
import hr.asyncro.mac2014.presenter.RecipePresenter;

/**
 * Created by David on 18.9.2014..
 */
public class CookDialog extends LightDialog implements AdapterView.OnItemClickListener {

    TextView txtDialogTitle;
    EditText etDialogSearch;
    ListView lvDialogList;
    private boolean recipe = false;

    private LightAdapter adapter;
    private OnIngredientSelectListener onIngredientSelectListener;
    private OnRecipeSelectListener onRecipeSelectListener;
    private IngredientPresenter selectedIngredient;

    public CookDialog(Context context) {
        super(context, R.layout.dialog_cook);
        init();
        getDialogBuilder().withEffect(Effectstype.Slideleft);
        getDialogBuilder().withDuration(400);
    }

    private void init() {
        adapter = new LightAdapter(getContext());
        txtDialogTitle = (TextView) findViewById(R.id.txtDialogIngredientsTitle);
        etDialogSearch = (EditText) findViewById(R.id.etDialogIngredientsSearch);
        lvDialogList = (ListView) findViewById(R.id.lvDialogIngredients);
        lvDialogList.setAdapter(adapter);
        lvDialogList.setOnItemClickListener(this);
    }

    public void populateDialog(Recipe recipe) {
        Concept[] concepts = recipe.getConcepts();
        Function[] functions = recipe.getFunctions();
        for (Concept concept : concepts) {
            IngredientPresenter presenter = new IngredientPresenter(concept);
            adapter.addItem(presenter);
        }
        for (Function function : functions) {
            IngredientPresenter presenter = new IngredientPresenter(function);
            adapter.addItem(presenter);
        }
        this.recipe = false;
        txtDialogTitle.setText(R.string.pick_ingredient);
    }

    public void populateDialog(Recipe[] recipes) {
        for (Recipe recipe : recipes)
            adapter.addItem(new RecipePresenter(recipe));
        this.recipe = true;
        txtDialogTitle.setText(R.string.pick_recipe);
    }

    public void clear() {
        adapter.clear();
    }

    public OnIngredientSelectListener getOnIngredientSelectListener() {
        return onIngredientSelectListener;
    }

    public void setOnIngredientSelectListener(OnIngredientSelectListener onIngredientSelectListener) {
        this.onIngredientSelectListener = onIngredientSelectListener;
    }

    public OnRecipeSelectListener getOnRecipeSelectListener() {
        return onRecipeSelectListener;
    }

    public void setOnRecipeSelectListener(OnRecipeSelectListener onRecipeSelectListener) {
        this.onRecipeSelectListener = onRecipeSelectListener;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (!recipe && onIngredientSelectListener != null) {
            IngredientPresenter addedPresenter = new IngredientPresenter((IngredientPresenter) adapter.getItem(i));
            addedPresenter.setRoot(selectedIngredient);
            selectedIngredient = null;
            onIngredientSelectListener.onIngredientSelect(addedPresenter);
        } else if (recipe && onRecipeSelectListener != null)
            onRecipeSelectListener.onRecipeSelect((RecipePresenter) adapter.getItem(i));
        this.dismiss();
    }

    public void setIngredientSelected(IngredientPresenter presenter) {
        this.selectedIngredient = presenter;
    }

    public interface OnIngredientSelectListener {
        public void onIngredientSelect(IngredientPresenter ingredientPresenter);
    }

    public interface OnRecipeSelectListener {
        public void onRecipeSelect(RecipePresenter recipePresenter);
    }

}
