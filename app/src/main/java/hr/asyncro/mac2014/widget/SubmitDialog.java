package hr.asyncro.mac2014.widget;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dmacan.lightandroid.ui.widget.LightDialog;

import hr.asyncro.mac2014.R;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class SubmitDialog extends LightDialog implements View.OnClickListener {

    private EditText submitName;
    private Button submitRecipe;
    private OnButtonClickListener onButtonClickListener;

    public SubmitDialog(Context context) {
        super(context, R.layout.dialog_submit);
        init();
    }

    private void init() {
        submitName = (EditText) findViewById(R.id.etDialogName);
        submitRecipe = (Button) findViewById(R.id.btDialogSubmitOk);
        submitRecipe.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btDialogSubmitOk:
                if (onButtonClickListener != null) {
                    onButtonClickListener.onButtonClickListener(submitName.getText().toString());
                    this.dismiss();
                }
                break;
        }

    }

    public OnButtonClickListener getOnButtonClickListener() {
        return onButtonClickListener;
    }

    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener) {
        this.onButtonClickListener = onButtonClickListener;
    }

    public interface OnButtonClickListener {
        public void onButtonClickListener(String user);
    }
}
