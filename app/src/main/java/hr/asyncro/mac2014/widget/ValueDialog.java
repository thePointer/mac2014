package hr.asyncro.mac2014.widget;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dmacan.lightandroid.ui.widget.LightDialog;

import hr.asyncro.mac2014.R;

/**
 * Created by David on 18.9.2014..
 */
public class ValueDialog extends LightDialog implements View.OnClickListener {

    EditText etValue;
    Button btnConfirm;
    private OnValueConfirmListener onValueConfirmListener;

    public ValueDialog(Context context) {
        super(context, R.layout.dialog_value);
        init();
    }

    private void init() {
        etValue = (EditText) findViewById(R.id.etDialogValue);
        btnConfirm = (Button) findViewById(R.id.btnDialogConfirm);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        dismiss();
        if (onValueConfirmListener != null)
            onValueConfirmListener.onValueConfirm(etValue.getText().toString());
    }

    public OnValueConfirmListener getOnValueConfirmListener() {
        return onValueConfirmListener;
    }

    public void setOnValueConfirmListener(OnValueConfirmListener onValueConfirmListener) {
        this.onValueConfirmListener = onValueConfirmListener;
    }

    @Override
    public void show() {
        super.show();
        etValue.setText("");
    }

    public interface OnValueConfirmListener {
        public void onValueConfirm(String value);
    }
}
