package hr.asyncro.mac2014.widget;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.dmacan.lightandroid.ui.widget.LightDialog;
import com.dmacan.lightandroid.util.LightFont;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;

import hr.asyncro.mac2014.R;
import hr.asyncro.mac2014.presenter.IngredientPresenter;

/**
 * Created by David on 18.9.2014..
 */
public class ChooseActionDialog extends LightDialog implements View.OnClickListener {

    private TextView icAdd;
    private TextView icDelete;
    private TextView icWrite;
    private IngredientPresenter selectedPresenter;

    private OnActionChosenListener onActionChosenListener;

    public ChooseActionDialog(Context context) {
        super(context, R.layout.dialog_action_choose);
        init();
        getDialogBuilder().withEffect(Effectstype.Flipv);
        getDialogBuilder().withDuration(400);
    }

    private void init() {
        icAdd = (TextView) findViewById(R.id.txtActionAdd);
        icDelete = (TextView) findViewById(R.id.txtActionDelete);
        icWrite = (TextView) findViewById(R.id.txtActionEdit);
        icAdd.setOnClickListener(this);
        icDelete.setOnClickListener(this);
        icWrite.setOnClickListener(this);
        LightFont.setFont(icAdd, icDelete, icWrite);
    }

    public IngredientPresenter getSelectedPresenter() {
        return selectedPresenter;
    }

    public void setSelectedPresenter(IngredientPresenter selectedPresenter) {
        this.selectedPresenter = selectedPresenter;
    }

    @Override
    public void onClick(View view) {
        dismiss();
        if (onActionChosenListener != null) {
            switch (view.getId()) {
                case R.id.txtActionAdd:
                    onActionChosenListener.onActionChosen(Action.ADD);
                    break;
                case R.id.txtActionEdit:
                    onActionChosenListener.onActionChosen(Action.WRITE);
                    break;
                case R.id.txtActionDelete:
                    onActionChosenListener.onActionChosen(Action.DELETE);
                    break;
            }
        }
    }

    public OnActionChosenListener getOnActionChosenListener() {
        return onActionChosenListener;
    }

    public void setOnActionChosenListener(OnActionChosenListener onActionChosenListener) {
        this.onActionChosenListener = onActionChosenListener;
    }

    public enum Action {
        ADD, DELETE, WRITE
    }

    public interface OnActionChosenListener {
        public void onActionChosen(Action action);
    }

}
