package hr.asyncro.mac2014.widget;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class MACWebViewClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
