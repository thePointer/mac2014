package hr.asyncro.mac2014.widget;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dmacan.lightandroid.ui.widget.LightDialog;

import hr.asyncro.mac2014.R;

/**
 * Created by ahuskano on 9/19/2014.
 */
public class InfoDialog extends LightDialog implements View.OnClickListener {

    private OnInfoConfirmListener onInfoConfirmListener;
    private Button confirmButton;
    private TextView description;

    public InfoDialog(Context context) {
        super(context, R.layout.dialog_info);
        init();
    }

    private void init() {
        confirmButton = (Button) findViewById(R.id.btnDialogInfoConfirm);
        confirmButton.setOnClickListener(this);
        description=(TextView) findViewById(R.id.tvDialogInfoDescription);
    }

    public void setInfo(String info){
        description.setText(info);
    }

    @Override
    public void onClick(View view) {
        if (onInfoConfirmListener != null)
            onInfoConfirmListener.onInfoConfirmListener();
    }

    public OnInfoConfirmListener getOnInfoConfirmListener() {
        return onInfoConfirmListener;
    }

    public void setOnInfoConfirmListener(OnInfoConfirmListener onInfoConfirmListener) {
        this.onInfoConfirmListener = onInfoConfirmListener;
    }

    public interface OnInfoConfirmListener {
        public void onInfoConfirmListener();
    }

}
