package hr.asyncro.mac2014.data;

import com.dmacan.lightandroid.type.LightData;
import com.google.gson.annotations.Expose;

/**
 * Created by ahuskano on 9/17/2014.
 */
public class Concept extends LightData {

    @Expose
    private int id;

    @Expose
    private String name;

    @Expose
    private String description;

    @Expose
    private int category;

    @Expose
    private String createdAt;

    @Expose
    private String updatedAt;

    @Expose
    private String example;

    private int value = 1;


    public Concept() {
    }

    public Concept(Concept concept) {
        this.id = concept.getId();
        this.name = concept.getName();
        this.description = concept.getDescription();
        this.category = concept.getCategory();
        this.createdAt = concept.getCreatedAt();
        this.updatedAt = concept.getUpdatedAt();
        this.example = concept.getExample();
    }

    public Concept(int id, String name, String description, int category, String createdAt, String updatedAt, String example) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.example = example;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCategoryName() {
        switch (this.category) {
            case Category.INGREDIENT:
                return "Ingredient";
            case Category.CONCEPT:
                return "Concept";
            default:
                return "Unknown";
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static class Category {
        public static final int INGREDIENT = 1;
        public static final int CONCEPT = 0;
    }
}
