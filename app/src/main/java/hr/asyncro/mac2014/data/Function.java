package hr.asyncro.mac2014.data;

import com.dmacan.lightandroid.type.LightData;
import com.google.gson.annotations.Expose;

/**
 * Created by ahuskano on 9/17/2014.
 */
public class Function extends LightData {

    @Expose
    private String functionName;

    @Expose
    private int usageCount;

    @Expose
    private boolean order;

    public Function() {
    }

    public Function(Function function) {
        this.functionName = function.getFunctionName();
        this.usageCount = function.getUsageCount();
        this.order = function.isOrder();
    }

    public Function(String functionName, int usageCount, boolean order) {
        this.functionName = functionName;
        this.usageCount = usageCount;
        this.order = order;
    }

    public boolean isOrder() {
        return order;
    }

    public void setOrder(boolean order) {
        this.order = order;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }
}
