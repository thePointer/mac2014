package hr.asyncro.mac2014.data.responses;

import com.dmacan.lightandroid.api.LightResponse;
import com.google.gson.annotations.Expose;

import hr.asyncro.mac2014.data.Concept;

/**
 * Created by ahuskano on 9/17/2014.
 */
public class ConceptResponse extends LightResponse {

    @Expose
    private Concept data;

    public ConceptResponse() {
    }

    public ConceptResponse(Concept data) {
        this.data = data;
    }

    public Concept getData() {
        return data;
    }

    public void setData(Concept data) {
        this.data = data;
    }
}
