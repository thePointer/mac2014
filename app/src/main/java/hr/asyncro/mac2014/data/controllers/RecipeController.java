package hr.asyncro.mac2014.data.controllers;

import com.dmacan.lightandroid.data.LightController;
import com.dmacan.lightandroid.util.LightAPIUtil;

import java.util.List;

import hr.asyncro.mac2014.api.MacAPI;
import hr.asyncro.mac2014.data.Concept;
import hr.asyncro.mac2014.data.Function;
import hr.asyncro.mac2014.data.Recipe;
import hr.asyncro.mac2014.data.requests.SubmitRecipeRequest;
import hr.asyncro.mac2014.data.responses.RecipeResponse;
import hr.asyncro.mac2014.data.responses.RecipeSubmitionResponse;
import hr.asyncro.mac2014.data.responses.RecipesResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class RecipeController extends LightController {

    private Callback<RecipeSubmitionResponse> submitRecipeCallback = new Callback<RecipeSubmitionResponse>() {
        @Override
        public void success(RecipeSubmitionResponse recipeSubmitionResponse, Response response) {
            if (getOnDataReadListener() != null)
                getOnDataReadListener().onDataRead(recipeSubmitionResponse);
        }

        @Override
        public void failure(RetrofitError error) {
            if (getOnErrorListener() != null)
                getOnErrorListener().onError(error);
        }
    };

    private Callback<RecipeResponse> readRecipeCallback = new Callback<RecipeResponse>() {
        @Override
        public void success(RecipeResponse recipeResponse, Response response) {
            if (getOnDataReadListener() != null)
                getOnDataReadListener().onDataRead(recipeResponse);
        }

        @Override
        public void failure(RetrofitError error) {
            if (getOnErrorListener() != null)
                getOnErrorListener().onError(error);

        }
    };

    private Callback<RecipesResponse> readRecipesCallback = new Callback<RecipesResponse>() {
        @Override
        public void success(RecipesResponse recipesResponse, Response response) {
            if (getOnDataReadListener() != null)
                getOnDataReadListener().onDataRead(recipesResponse);

        }

        @Override
        public void failure(RetrofitError error) {
            if (getOnErrorListener() != null)
                getOnErrorListener().onError(error);
        }
    };

    public void readRecipe(int id) {
        LightAPIUtil.getRestAdapter(MacAPI.API_LOCATION).create(MacAPI.class).getRecipe(id, readRecipeCallback);

    }

    public void readRecipes() {
        LightAPIUtil.getRestAdapter(MacAPI.API_LOCATION).create(MacAPI.class).getRecipes(readRecipesCallback);

    }


    public void submitRecipe(Recipe chosenRecipe, List<Function> functions, List<Concept> concepts, String user) {
        Recipe recipe = getRecipe(chosenRecipe, functions, concepts);
        SubmitRecipeRequest request = new SubmitRecipeRequest();
        request.setMethod("submitRecipe");
        request.setUser(user);
        request.setData(recipe);
        LightAPIUtil.getRestAdapter(MacAPI.API_LOCATION).create(MacAPI.class).submitRecipe(request, submitRecipeCallback);
    }

    private Recipe getRecipe(Recipe chosenRecipe, List<Function> functions, List<Concept> concepts) {
        Recipe recipe = new Recipe();
        recipe.setName(chosenRecipe.getName());
        recipe.setId(chosenRecipe.getId());
        recipe.setDescription(chosenRecipe.getDescription());
        Function[] recipeFunctions = new Function[functions.size()];
        Concept[] recipeConcepts = new Concept[concepts.size()];
        for (int i = 0; i < functions.size(); i++) {
            recipeFunctions[i] = functions.get(i);
        }
        for (int i = 0; i < concepts.size(); i++) {
            recipeConcepts[i] = concepts.get(i);
        }
        recipe.setConcepts(recipeConcepts);
        recipe.setFunctions(recipeFunctions);
        return recipe;
    }

}
