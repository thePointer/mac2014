package hr.asyncro.mac2014.data.responses;

import com.dmacan.lightandroid.api.LightResponse;
import com.google.gson.annotations.Expose;

import hr.asyncro.mac2014.data.Recipe;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class RecipeResponse extends LightResponse {

    @Expose
    private Recipe data;

    public RecipeResponse() {
    }

    public RecipeResponse(Recipe data) {
        this.data = data;
    }

    public Recipe getData() {
        return data;
    }

    public void setData(Recipe data) {
        this.data = data;
    }
}
