package hr.asyncro.mac2014.data.requests;

import com.dmacan.lightandroid.api.LightRequest;
import com.google.gson.annotations.Expose;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class SubmitRecipeRequest extends LightRequest {

    @Expose
    private String user;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
