package hr.asyncro.mac2014.data;

import com.dmacan.lightandroid.type.LightData;
import com.google.gson.annotations.Expose;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class Recipe extends LightData {

    @Expose
    private int id;

    @Expose
    private String name;

    @Expose
    private String description;

    @Expose
    private Function[] functions;

    @Expose
    private Concept[] concepts;

    public Recipe() {
    }

    public Recipe(int id, String name, String description, Function[] functions, Concept[] concepts) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.functions = functions;
        this.concepts = concepts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Function[] getFunctions() {
        return functions;
    }

    public void setFunctions(Function[] functions) {
        this.functions = functions;
    }

    public Concept[] getConcepts() {
        return concepts;
    }

    public void setConcepts(Concept[] concepts) {
        this.concepts = concepts;
    }
}
