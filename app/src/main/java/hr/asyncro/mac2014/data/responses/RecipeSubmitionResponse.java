package hr.asyncro.mac2014.data.responses;

import com.dmacan.lightandroid.api.LightResponse;
import com.google.gson.annotations.Expose;

/**
 * Created by ahuskano on 9/18/2014.
 */
public class RecipeSubmitionResponse extends LightResponse {

    @Expose
    private int points;

    public RecipeSubmitionResponse() {
    }

    public RecipeSubmitionResponse(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
