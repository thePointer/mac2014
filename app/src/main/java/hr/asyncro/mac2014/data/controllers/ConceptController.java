package hr.asyncro.mac2014.data.controllers;

import com.dmacan.lightandroid.data.LightController;
import com.dmacan.lightandroid.util.LightAPIUtil;

import hr.asyncro.mac2014.api.MacAPI;
import hr.asyncro.mac2014.data.responses.ConceptResponse;
import hr.asyncro.mac2014.data.responses.ConceptsResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ahuskano on 9/17/2014.
 */
public class ConceptController extends LightController {

    private Callback<ConceptsResponse> readConceptsCallback = new Callback<ConceptsResponse>() {

        @Override
        public void success(ConceptsResponse concepts, Response response) {
            if (getOnDataReadListener() != null) {
                getOnDataReadListener().onDataRead(concepts);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if (getOnErrorListener() != null) {
                getOnErrorListener().onError(error);
            }

        }
    };
    private Callback<ConceptResponse> readConceptCallback = new Callback<ConceptResponse>() {
        @Override
        public void success(ConceptResponse concept, Response response) {
            if (getOnDataReadListener() != null) {
                getOnDataReadListener().onDataRead(concept);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if (getOnErrorListener() != null) {
                getOnErrorListener().onError(error);
            }
        }
    };

    public void readConcepts() {
        LightAPIUtil.getRestAdapter(MacAPI.API_LOCATION).create(MacAPI.class).getConcepts(readConceptsCallback);

    }

    public void readConcept(int id) {
        LightAPIUtil.getRestAdapter(MacAPI.API_LOCATION).create(MacAPI.class).getConcept(id, readConceptCallback);
    }
}
