package com.dmacan.lightandroid.ui.widget;

import android.content.Context;
import android.view.View;

import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

/**
 * Created by David on 18.9.2014..
 */
public class LightDialog {

    private int layout;
    private Context context;
    private NiftyDialogBuilder dialogBuilder;

    public LightDialog(Context context, int layout) {
        this.context = context;
        this.layout = layout;
        init();
        dialogBuilder.setCustomView(layout, context);
        dialogBuilder.withDuration(700);
    }

    public LightDialog(View view) {
        this.context = view.getContext();
        init();
        dialogBuilder.setCustomView(view, context);
    }

    public LightDialog(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        this.dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder.withMessage(null);
        dialogBuilder.withTitle(null);
    }

    public void show() {
        dialogBuilder.show();
    }

    public void dismiss() {
        dialogBuilder.dismiss();
    }

    public View findViewById(int id) {
        return dialogBuilder.findViewById(id);
    }

    public Context getContext() {
        return context;
    }

    public NiftyDialogBuilder getDialogBuilder() {
        return dialogBuilder;
    }


}
